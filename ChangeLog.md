ChangeLog
=========


0.9.4 (Oct, 2023)
-----------------
* Update continuous coarse graining
* Refactorize code for basin transitions in pruning
* Add rikin_pipeline script
* Rename tool to RIkin
* Doxygen awesome style for generated documentation
* Extend documentation

0.9.3 (June, 2022)
------------------
* Update / fix reading of binary from octave

0.9.2 (Feb, 2017)
-------------------

* make rikin_prune argument only-first-component optional

0.9.1 (Feb, 2017)
-------------------

* code refactorization
* bug fixes

0.9 (Apr, 2016)
----------------

* connect disconnected components heuristically
* add pre-exponential factor for transitions from and to first state
* [=open state] in rrikin_prune
