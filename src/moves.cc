#include "hybrid_ensemble_model.hh"

/**
 * @file moves.cc
 *
 * @brief Moves and Move Iteration for HybEnsModel
 *
 * @todo check limits for all moves
 * @todo check symmetry of move set
 */

//! whether shift moves are allowed
bool allow_shift_move=true;

// ------------------------------------------------------------
// Move
HybEnsModel::Move::~Move() {}


std::ostream &
HybEnsModel::Move::print(std::ostream &out) const {
    out << "[Generic Move]";
    return out;
}

// ------------------------------------------------------------
// Grow-Shrink Move

HybEnsModel::GrowShrinkMove::GrowShrinkMove(const MoveIterator &mi): Move(mi) {}

HybEnsModel::GrowShrinkMove::~GrowShrinkMove() {}

std::ostream &
HybEnsModel::GrowShrinkMove::print(std::ostream &out) const {
    out << "[Grow Shrink Move "
	<<k1<<" "<<k2 //<<" "
	// <<min1<<" "<<min2<<" "
	// <<i1<<" "<<i2 <<" "
	// <<max1<<" "<<max2
	<<"]";
    return out;
}


bool
HybEnsModel::GrowShrinkMove::firstLeft() {        
    // try add/remove largest loop to the left
    k1 = min1;
    k2 = min2;
    
    return (k1<i1 && k2<i2);
}
    
bool
HybEnsModel::GrowShrinkMove::firstRight() {        
    // try add/remove smallest loop to the right
    k1 = std::min(max1,i1+1);
    k2 = std::min(max2,i2+1);
    
    // succeed, if there is a move to the right, otherwise fail
    return (k1>i1 && k2>i2);
}

bool
HybEnsModel::GrowShrinkMove::first(size_t i1_,
				   size_t i2_,
				   size_t min1_,
				   size_t min2_,
				   size_t max1_,
				   size_t max2_) {
    i1=i1_;
    i2=i2_;
    min1=min1_;
    min2=min2_;
    max1=max1_;
    max2=max2_;

    const HybEnsModel &m=mi.model();
        
    // constrain max and min by max loop size
    min1 = std::max(min1+m.maxunpinloop()+1,i1)-m.maxunpinloop()-1;
    min2 = std::max(min2+m.maxunpinloop()+1,i2)-m.maxunpinloop()-1;
    max1 = std::min(max1,i1+m.maxunpinloop()+1);
    max2 = std::min(max2,i2+m.maxunpinloop()+1);
    
    return firstLeft() || firstRight();
}

bool
HybEnsModel::GrowShrinkMove::nextLeft() {
    assert(k1<i1);
       
    // mode "grow/shrink to left"
    
    k2++;
    if (k2<i2) {
	return true;
    } else {
	k1++;
	k2=min2;
	return (k1<i1);
    }
}

bool
HybEnsModel::GrowShrinkMove::nextRight() {
    assert(k1>i1);

    // mode "grow/shrink to right"
    
    assert(max2 > i2); // this code is accessed only after success of first or
    // mode switch in next, in both cases, the condition is implied
    
    k2++;
    if (k2<=max2) {
	return true;
    } else {
	k1++;
	k2 = i2+1;
	
	return (k1<=max1);
    }
}

bool
HybEnsModel::GrowShrinkMove::next() {        
    if (k1<i1) {
	return nextLeft() || firstRight();
    } else {
	return nextRight();
    }
}

HybEnsModel::energy_t
HybEnsModel::GrowShrinkMove::transitionEnergy(size_t site, size_t left) const {
    
    const StateDescription &sd=mi.origin();
    StateDescription sd2(sd);
    apply(sd2);
    
    bool grow=( (left && k1<i1) || (!left && k1>i1) );

    const StateDescription &sd_small=grow ? sd  : sd2;
    const StateDescription &sd_large=grow ? sd2 : sd ;
    
    return
	left
	?
	transitionEnergy(sd_small,sd_large,k1,k2,i1,i2)
	:
	transitionEnergy(sd_small,sd_large,i1,i2,k1,k2);
}

HybEnsModel::energy_t
HybEnsModel::GrowShrinkMove::transitionEnergy(const StateDescription &sd_small,
					      const StateDescription &sd_large,
					      size_t loop_i1, size_t loop_i2, size_t loop_j1, size_t loop_j2
					      ) const {
    // assert some of the preconditions
    assert(sd_small.size()!=0);
    assert(sd_large.size()==sd_small.size());
    
    
    //std::cout <<std::endl << "GrowShrinkMove::transitionEnergy" << sd_small << " " << sd_large<< " " 
    // 	      << loop_i1 << " "<< loop_i2 << " " << loop_j1 << " " << loop_j2
    // 	      <<std::endl;
    
    
    const HybEnsModel &model = mi.model();
    
    
    // pf of transition state all sites without added/removed loop
    energy_t e_hyb =
	model.energy_hybrid(sd_small[0])
	+
	((sd_small.size()==2)?model.energy_hybrid(sd_small[1]):0);
    
    // Energy of added hybridization loop
    if (loop_i1>loop_j1) {
	std::swap(loop_i1,loop_j1);
	std::swap(loop_i2,loop_j2);
    }
    energy_t e_loop = model.energy_hybrid_loop(loop_i1,loop_i2,loop_j1,loop_j2);

    // energy difference for unpairing
    energy_t e_unp =
	(sd_small.size()==1)
	?
	model.energy_unpair(sd_large[0])
	:
	model.energy_unpair(sd_large[0],
			    sd_large[1]);

    //std::cout << "GrowShrinkMove::transitionEnergy returns " << e_hyb <<" + " << e_loop <<" + "<< e_unp << std::endl;

    return
	e_hyb
	+
	e_loop
	+
	e_unp;
}


// ------------------------------------------------------------
// Grow-Shrink Move First site Left end

HybEnsModel::GrowShrinkMoveFL::GrowShrinkMoveFL(const MoveIterator &mi):GrowShrinkMove(mi) {}

HybEnsModel::GrowShrinkMoveFL::~GrowShrinkMoveFL() {}

HybEnsModel::Move *
HybEnsModel::GrowShrinkMoveFL::nextMoveType() const {
    return new GrowShrinkMoveFR(mi);
}

bool
HybEnsModel::GrowShrinkMoveFL::first() {
    
    // set to first move of this type if there is one
    
    const StateDescription &o=mi.origin();
    const HybEnsModel &m=mi.model();

    // fail if there is no site
    if (o.size() == 0) {
	return false;
    }

    return 
	HybEnsModel::GrowShrinkMove::first(o[0].i1,
					   o[0].i2, 
					   1, 
					   1,
					   o[0].j1-m.minsitesize()+1,
					   o[0].j2-m.minsitesize()+1
					   );
}

HybEnsModel::energy_t
HybEnsModel::GrowShrinkMoveFL::transitionEnergy() const {
    return
	HybEnsModel::GrowShrinkMove::transitionEnergy(0,true);
}
    

void
HybEnsModel::GrowShrinkMoveFL::apply(StateDescription &sd) const {
    sd[0].i1=k1;
    sd[0].i2=k2;
}

// ------------------------------------------------------------
// Grow-Shrink Move First site Right end


HybEnsModel::GrowShrinkMoveFR::GrowShrinkMoveFR(const MoveIterator &mi):GrowShrinkMove(mi) {}

HybEnsModel::GrowShrinkMoveFR::~GrowShrinkMoveFR() {}

HybEnsModel::Move *
HybEnsModel::GrowShrinkMoveFR::nextMoveType() const {
    return new GrowShrinkMoveSL(mi);
}

bool
HybEnsModel::GrowShrinkMoveFR::first() {
    
    // set to first move of this type if there is one
    const StateDescription &o=mi.origin();
    const HybEnsModel &m=mi.model();
    
    // fail if there is no site
    if (o.size() == 0) {
	return false;
    }
    
    size_t len1 = m.seqA().length();
    size_t len2 = m.seqB().length();

    return 
	HybEnsModel::GrowShrinkMove::first(
					   o[0].j1,
					   o[0].j2, 
					   o[0].i1+m.minsitesize()-1,
					   o[0].i2+m.minsitesize()-1,
					   (o.size()==2)
					   ? o[1].i1-m.minsitedist()-1
					   : len1,
					   (o.size()==2)
					   ? o[1].i2-m.minsitedist()-1
					   : len2
					   );
}

HybEnsModel::energy_t
HybEnsModel::GrowShrinkMoveFR::transitionEnergy() const {
    return
	HybEnsModel::GrowShrinkMove::transitionEnergy(0,false);
}

void
HybEnsModel::GrowShrinkMoveFR::apply(StateDescription &sd) const {
    sd[0].j1=k1;
    sd[0].j2=k2;
}

// ------------------------------------------------------------
// Grow-Shrink Move Second site Left end

HybEnsModel::GrowShrinkMoveSL::GrowShrinkMoveSL(const MoveIterator &mi):GrowShrinkMove(mi) {}

HybEnsModel::GrowShrinkMoveSL::~GrowShrinkMoveSL() {}

HybEnsModel::Move *
HybEnsModel::GrowShrinkMoveSL::nextMoveType() const {
    return new GrowShrinkMoveSR(mi);
}

bool
HybEnsModel::GrowShrinkMoveSL::first() {
    
    // set to first move of this type if there is one
    
    const StateDescription &o=mi.origin();
    const HybEnsModel &m=mi.model();

    // fail if there are less than two sites
    if (o.size() < 2) {
	return false;
    }
    
    return 
	HybEnsModel::GrowShrinkMove::first(
					   o[1].i1,
					   o[1].i2, 
					   o[0].j1+m.minsitedist()+1,
					   o[0].j2+m.minsitedist()+1,
					   o[1].j1-m.minsitesize()+1,
					   o[1].j2-m.minsitesize()+1
					   );
}

HybEnsModel::energy_t
HybEnsModel::GrowShrinkMoveSL::transitionEnergy() const {
    return
	HybEnsModel::GrowShrinkMove::transitionEnergy(1,true);
}

void
HybEnsModel::GrowShrinkMoveSL::apply(StateDescription &sd) const {
    sd[1].i1 = k1;
    sd[1].i2 = k2;
}

// ------------------------------------------------------------
// Grow-Shrink Move Second site Right end

HybEnsModel::GrowShrinkMoveSR::GrowShrinkMoveSR(const MoveIterator &mi):GrowShrinkMove(mi) {}

HybEnsModel::GrowShrinkMoveSR::~GrowShrinkMoveSR() {}

HybEnsModel::Move *
HybEnsModel::GrowShrinkMoveSR::nextMoveType() const {
    if (allow_shift_move)
	return new ShiftMove(mi);
    else
	return new RemoveSiteMove(mi);
}

bool
HybEnsModel::GrowShrinkMoveSR::first() {
    
    // set to first move of this type if there is one

    const StateDescription &o=mi.origin();
    const HybEnsModel &m=mi.model();

    size_t len1 = m.seqA().length();
    size_t len2 = m.seqB().length();
    
    // fail if there are less then two sites
    if (o.size() < 2)  {
	return false;
    }
    
    return 
	HybEnsModel::GrowShrinkMove::first(
					   o[1].j1,
					   o[1].j2, 
					   o[1].i1+m.minsitesize()+1,
					   o[1].i2+m.minsitesize()+1,
					   len1,
					   len2
					   );
}

HybEnsModel::energy_t
HybEnsModel::GrowShrinkMoveSR::transitionEnergy() const {    
    return
	HybEnsModel::GrowShrinkMove::transitionEnergy(1,false);
}

void
HybEnsModel::GrowShrinkMoveSR::apply(StateDescription &sd) const {
    sd[1].j1=k1;
    sd[1].j2=k2;
}

// ------------------------------------------------------------
// Shift Move
//

HybEnsModel::ShiftMove::ShiftMove(const MoveIterator &mi,size_t seq,size_t site,bool left): Move(mi),seq_(seq),site_(site),left_(left) {}

HybEnsModel::ShiftMove::~ShiftMove() {}

std::ostream &
HybEnsModel::ShiftMove::print(std::ostream &out) const {
    out << "[Shift Move "<<k_<<" ("<<seq_<<" "<<site_<<" "<<left_<<")]";
    return out;
}

HybEnsModel::Move *
HybEnsModel::ShiftMove::nextMoveType() const {

    size_t seq=seq_;
    size_t site=site_;
    bool left=left_;
    
    left=!left;
    if (left) {
	site++;
    }
    if (site>=mi.origin().size()) {
	site=0;
	seq++;
    }
    if (seq==2) {
	return new RemoveSiteMove(mi);
    }

    return new ShiftMove(mi,seq,site,left);
}

bool
HybEnsModel::ShiftMove::first() {
    // set to first move of this type if there is one
    
    const StateDescription &o=mi.origin();
    const HybEnsModel &m=mi.model();

    const size_t maxshiftmovesize=m.maxunpinloop()/2;
    
    // fail if there are not enough sites
    if (site_ >= o.size()) {
	return false;
    }
    
    i_ = o[site_].end(seq_,left_);
    
    // compute minimum k_
    
    //     at least 1
    k_ = 1;
    
    //     subtract at most maxshiftmovesize
    k_ = std::max(k_+maxshiftmovesize,i_)-maxshiftmovesize; //!avoid wrap
    
    //     if right end, respect minimum size of site
    if (!left_) {
	k_ = std::max(k_,o[site_].end(seq_,!left_)+m.minsitesize());
    }
    
    //     if left end of second site, don't touch other site
    if (site_==1 && left_) {
	k_ = std::max(k_,o[1-site_].end(seq_,!left_) + m.minsitedist());
    }
    
    // compute maximum k_ (i.e. maxk_)
    
    //     at most sequence length
    maxk_ =  (seq_==0)?m.seqA().length():m.seqB().length();
    
    //    add at most maxshiftmovesize
    maxk_ = std::min(maxk_,i_+maxshiftmovesize);
    
    //    if left end: min site size
    if (left_) {
	maxk_ = std::min(maxk_,o[site_].end(seq_,!left_) - m.minsitesize());
    }

    //    if right end of first site: min site diff 
    if (!left_ && site_==0 && o.size()>1) {
	maxk_ = std::min(maxk_,o[1-site_].end(seq_,!left_) - m.minsitedist());
    }
    
    if (k_==i_) k_++;
    return k_<=maxk_;
}

bool
HybEnsModel::ShiftMove::next() {
    k_++; // increase k_
    if (k_==i_) k_++; // skip original position i_

    return (k_<=maxk_); // true while <=maxk_
}

HybEnsModel::energy_t
HybEnsModel::ShiftMove::transitionEnergy() const {
    
    // simple definition of transition state: 
    // unpair energy for larger state + hybridization energy of smaller state

    const HybEnsModel &model = mi.model();

    const StateDescription &sd=mi.origin();
    StateDescription sd2(sd);
    apply(sd2);
    
    // determine smaller and larger of source and target
    bool grow=( (left_ && k_<i_) || (!left_ && k_>i_) );
    const StateDescription &sd_small=grow ? sd  : sd2;
    const StateDescription &sd_large=grow ? sd2 : sd ;
    

    energy_t e_hyb =
	model.energy_hybrid(sd_small[site_]);
    
    if (sd_small.size()==2) {
	e_hyb+=
	    model.energy_hybrid(sd_small[1-site_]);
    }
    
    // energy difference for unpairing
    energy_t e_unp =
	(sd.size()==1)
	?
	model.energy_unpair(sd_large[0])
	:
	model.energy_unpair(sd_large[0],
			    sd_large[1]);
    
    return e_hyb+e_unp;
}

void
HybEnsModel::ShiftMove::apply(StateDescription &sd) const {
    sd[site_].end(seq_,left_)=k_;
}


// ------------------------------------------------------------
// Move that removes a site
//

HybEnsModel::RemoveSiteMove::RemoveSiteMove(const MoveIterator &mi) : Move(mi) {}

HybEnsModel::RemoveSiteMove::~RemoveSiteMove() {}

std::ostream &
HybEnsModel::RemoveSiteMove::print(std::ostream &out) const {
    out << "[Remove Site Move "<<site<<"]";
    return out;
}


HybEnsModel::Move *
HybEnsModel::RemoveSiteMove::nextMoveType() const {
    return new NewSiteMoveF(mi);
}


bool
HybEnsModel::RemoveSiteMove::thisornext() {
    const HybEnsModel &m=mi.model();
       
    for(;site<mi.origin().size();site++) {
	// test whether it is allowed to remove the site,
	// and return true on success
	
	const StateDescription::ISite &is=mi.origin()[site];
	
	if ((is.j1-is.i1-1 <= m.maxunpinloop())
	    &&
	    (is.j2-is.i2-1 <= m.maxunpinloop()))
	    {
		return true;
	    }
    }
    return false;
}

bool
HybEnsModel::RemoveSiteMove::first() {
    site=0;
    
    return thisornext();
}

bool
HybEnsModel::RemoveSiteMove::next() {
    site++;
    return thisornext();
}

HybEnsModel::energy_t
HybEnsModel::RemoveSiteMove::transitionEnergy() const {
    // make removed site interaction loop and score
    const HybEnsModel &model = mi.model();

    assert(site<2);

    const StateDescription::ISite &is=mi.origin()[site];
    const StateDescription::ISite &is2=mi.origin()[1-site];
    
    energy_t E_unp =
	    (mi.origin().size()==1)
	    ?
	    model.energy_unpair(is)
	    :
	    model.energy_unpair(is, is2);
        	
    energy_t E_loop =
	model.energy_hybrid_loop(is.i1,is.i2,is.j1,is.j2);
    
    energy_t E_hyb = 0;
    if (mi.origin().size()==2) {
	E_hyb = model.energy_hybrid(is2);	    
    }
    
    return E_unp + E_loop + E_hyb;
}

void
HybEnsModel::RemoveSiteMove::apply(StateDescription &sd) const {
    assert(site<2);
    
    if (site==0) {
	if (sd.size()==2) {
	    sd[0]=sd[1];
	    
	}
	sd.resize(sd.size()-1);
    } else {
	sd.resize(1);
    }
}


// ------------------------------------------------------------
// Move that creates a new site
//

HybEnsModel::NewSiteMove::NewSiteMove(const MoveIterator &mi) : Move(mi) {
}

HybEnsModel::NewSiteMove::~NewSiteMove() {}

bool
HybEnsModel::NewSiteMove::first() {
    const HybEnsModel &model=mi.model();
    
    is.i1=mini1;
    is.i2=mini2;
    is.j1=is.i1+model.minsitesize()-1;
    is.j2=is.i2+model.minsitesize()-2;
    
    return (is.j1 <= maxj1 && is.j2+1<=maxj2)
	&&
	next();
}

bool
HybEnsModel::NewSiteMove::next() {
    const HybEnsModel &model=mi.model();
    
    is.j2++;
    if (is.j2 <= maxj2 && is.j2 <= is.i2+model.maxunpinloop()+1) {
	return true;
    }
    
    is.j1++;
    is.j2 = is.i2+model.minsitesize()-1;
    if ( is.j1 <= maxj1 && is.j1 <= is.i1+model.maxunpinloop()+1
	 && is.j2 <= maxj2) {
	return true;
    }
    
    is.i2++;
    is.j1 = is.i1+model.minsitesize()-1;
    is.j2 = is.i2+model.minsitesize()-1;
    if ( is.j1 <= maxj1 && is.j2 <= maxj2 ) {
	return true;
    }
    
    is.i1++;
    is.i2 = mini2;
    is.j1 = is.i1+model.minsitesize()-1;
    is.j2 = is.i2+model.minsitesize()-1;	
    return ( is.j1 <= maxj1  && is.j2 <= maxj2 );
}


std::ostream &
HybEnsModel::NewSiteMove::print(std::ostream &out) const {
    out << "[New Site Move "<<is.i1<<" "<<is.i2<<" "<<is.j1<<" "<<is.j2<<"]";
    return out;
}

// ------------------------------------------------------------
// Move that creates a new site as the first interaction site
//

HybEnsModel::NewSiteMoveF::NewSiteMoveF(const MoveIterator &mi) : NewSiteMove(mi) {
}

HybEnsModel::NewSiteMoveF::~NewSiteMoveF() {}



HybEnsModel::Move *
HybEnsModel::NewSiteMoveF::nextMoveType() const {
    Move *m = NULL;
    
    if (mi.with_double()) { 
	m = new NewSiteMoveL(mi);
    }
    return m;
}

bool
HybEnsModel::NewSiteMoveF::first() {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
       
    if (o.size()!=0) {
	return false;
    }
    
    mini1 = 1;
    mini2 = 1;
    maxj1 = model.seqA().length();
    maxj2 = model.seqB().length();
    
    return NewSiteMove::first();
}


HybEnsModel::energy_t
HybEnsModel::NewSiteMoveF::transitionEnergy() const {
    const HybEnsModel &model=mi.model();
        
    energy_t E_loop =
	model.energy_hybrid_loop(is.i1,is.i2,is.j1,is.j2);
    
    assert (mi.origin().size()==0);
    
    energy_t E_unp =
	model.energy_unpair(is);
    
    return E_unp + E_loop;
    
}

void
HybEnsModel::NewSiteMoveF::apply(StateDescription &sd) const {
    assert (sd.size()==0);
    
    sd.resize(1);
    sd[0]=is;
    
}


// ------------------------------------------------------------
// Move that creates a new site on the left
//

HybEnsModel::NewSiteMoveL::NewSiteMoveL(const MoveIterator &mi) : NewSiteMove(mi) {}

HybEnsModel::NewSiteMoveL::~NewSiteMoveL() {}

HybEnsModel::Move *
HybEnsModel::NewSiteMoveL::nextMoveType() const {
    return new NewSiteMoveR(mi);
}


bool
HybEnsModel::NewSiteMoveL::first() {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
       
    if (o.size()!=1) {
	return false;
    }
    
    mini1 = 1;
    mini2 = 1;
    
    // can we insert a site left of the single site
    if ( mini1 + model.minsitesize() - 1 + model.minsitedist() + 1 > o[0].i1 
	 ||
	 mini2 + model.minsitesize() - 1 + model.minsitedist() + 1 > o[0].i2
	 ) {
	return false;
    }
    
    maxj1 = o[0].i1 - model.minsitedist() - 1;
    maxj2 = o[0].i2 - model.minsitedist() - 1;
    
    return NewSiteMove::first();
}

HybEnsModel::energy_t
HybEnsModel::NewSiteMoveL::transitionEnergy() const {
    const StateDescription &o=mi.origin();
    assert(o.size()==1);
    
    const HybEnsModel &model=mi.model();
    
    const StateDescription::ISite &is_old=o[0];
    
    energy_t E_loop =
	model.energy_hybrid_loop(is.i1,is.i2,is.j1,is.j2);
    
    energy_t E_unp =
	model.energy_unpair(is_old);
    
    energy_t E_hyb = model.energy_hybrid(is_old);	    
    
    return E_unp + E_loop + E_hyb;
}

void
HybEnsModel::NewSiteMoveL::apply(StateDescription &sd) const {
    sd.resize(2);
    sd[1] = sd[0];
    sd[0] = is;
}

// ------------------------------------------------------------
// Move that creates a new site on the right
//

HybEnsModel::NewSiteMoveR::NewSiteMoveR(const MoveIterator &mi) : NewSiteMove(mi) {}

HybEnsModel::NewSiteMoveR::~NewSiteMoveR() {}

HybEnsModel::Move *
HybEnsModel::NewSiteMoveR::nextMoveType() const {
    return new MergeMove(mi);
}


bool
HybEnsModel::NewSiteMoveR::first() {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();

    size_t len1 = model.seqA().length();
    size_t len2 = model.seqB().length();
  
    if (o.size()!=1) {
	return false;
    }
    
    mini1 = o[0].j1 + model.minsitedist() + 1;
    mini2 = o[0].j2 + model.minsitedist() + 1;
    
    maxj1 = len1;
    maxj2 = len2;
    
    return NewSiteMove::first();
}

HybEnsModel::energy_t
HybEnsModel::NewSiteMoveR::transitionEnergy() const {
    const StateDescription &o=mi.origin();
    assert(o.size()==1);
    
    const HybEnsModel &model=mi.model();
    
    const StateDescription::ISite &is_old=o[0];
    
    energy_t E_loop =
	model.energy_hybrid_loop(is.i1,is.i2,is.j1,is.j2);
    
    energy_t E_unp =
	model.energy_unpair(is_old,is);
    
    energy_t E_hyb = model.energy_hybrid(is_old);	    
    
    return E_unp + E_loop + E_hyb;
}

void
HybEnsModel::NewSiteMoveR::apply(StateDescription &sd) const {
    sd.resize(2);
    sd[1]=is;
}



// ------------------------------------------------------------
// Move that merges two sites
//

HybEnsModel::MergeMove::MergeMove(const MoveIterator &mi) : Move(mi) {}

HybEnsModel::MergeMove::~MergeMove() {}

HybEnsModel::Move *
HybEnsModel::MergeMove::nextMoveType() const {
    return new SplitMove(mi);
}

bool
HybEnsModel::MergeMove::first() {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
    
    if (o.size()!=2) {
	return false;
    }
    
    return 
	( o[0].j1 + model.minsitedist() + 1 == o[1].i1 )
	&&
	( o[0].j2 + model.minsitedist() + 1 == o[1].i2 );
}

bool
HybEnsModel::MergeMove::next() {
    return false;
}

HybEnsModel::energy_t
HybEnsModel::MergeMove::transitionEnergy() const {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
    
    
    const StateDescription::ISite is_between=
	StateDescription::ISite(o[0].j1,o[0].j2,o[1].i1,o[1].i2);

    const StateDescription::ISite is_merged=
	StateDescription::ISite(o[0].i1,o[0].i2,o[1].j1,o[1].j2);
    
    energy_t E_unp =
	model.energy_unpair(is_merged);
    
    energy_t E_hyb =
	model.energy_hybrid(is_between) 
	+ model.energy_hybrid(mi.origin()[0]) 
	+ model.energy_hybrid(mi.origin()[1]);
    
    return E_unp + E_hyb;
}

void
HybEnsModel::MergeMove::apply(StateDescription &sd) const {   
    const StateDescription::ISite is_merged=
	StateDescription::ISite(sd[0].i1,sd[0].i2,sd[1].j1,sd[1].j2);
    
    sd[0] = is_merged;
    sd.resize(1);
}

std::ostream &
HybEnsModel::MergeMove::print(std::ostream &out) const {
    out << "[Merge Move]";
    return out;
}

// ------------------------------------------------------------
// Move that splits one site into two
//

HybEnsModel::SplitMove::SplitMove(const MoveIterator &mi) : Move(mi) {}

HybEnsModel::SplitMove::~SplitMove() {}

std::ostream &
HybEnsModel::SplitMove::print(std::ostream &out) const {
    out << "[Split Move "<<i1<<" "<<i2<<"]";
    return out;
}


HybEnsModel::Move *
HybEnsModel::SplitMove::nextMoveType() const {
    return NULL;
}

bool
HybEnsModel::SplitMove::first() {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
    
    if (o.size()!=1) {
	return false;
    }
    
    i1 = o[0].i1 + model.minsitesize() - 1;
    i2 = o[0].i2 + model.minsitesize() - 1;
    
    return 
	( i1 + model.minsitedist() + model.minsitesize() <= o[0].j1 )
	&&
	( i2 + model.minsitedist() + model.minsitesize() <= o[0].j2 );
}

bool
HybEnsModel::SplitMove::next() {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
        
    i2++;
    if (i2 + model.minsitedist() + model.minsitesize() <= o[0].j2) {
	return true;
    } else {
	i1++;
	i2=o[0].i2 + model.minsitesize() - 1;
	return (i1 + model.minsitedist() + model.minsitesize() <= o[0].j1);
    }
}

HybEnsModel::energy_t
HybEnsModel::SplitMove::transitionEnergy() const {
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
        
    const StateDescription::ISite &is_orig = o[0];

    const StateDescription::ISite is_between=
	StateDescription::ISite(i1,i2,
				i1+model.minsitedist()+1,
				i2+model.minsitedist()+1);

    const StateDescription::ISite is1=
	StateDescription::ISite(is_orig.i1,is_orig.i2,is_between.i1,is_between.i2);

    const StateDescription::ISite is2=
	StateDescription::ISite(is_between.j1,is_between.j2,is_orig.j1,is_orig.j2);
    
    energy_t E_unp =
	model.energy_unpair(is_orig);
    
    energy_t E_hyb =
	model.energy_hybrid(is_between) 
	+ model.energy_hybrid(is1) 
	+ model.energy_hybrid(is2);
    
    return E_unp + E_hyb;
}

void
HybEnsModel::SplitMove::apply(StateDescription &sd) const {
    
    const HybEnsModel &model=mi.model();
    const StateDescription &o=mi.origin();
    const StateDescription::ISite &is_orig = o[0];

    const StateDescription::ISite is_between=
	StateDescription::ISite(i1,i2,
				i1+model.minsitedist()+1,
				i2+model.minsitedist()+1);

    const StateDescription::ISite is1=
	StateDescription::ISite(is_orig.i1,is_orig.i2,is_between.i1,is_between.i2);

    const StateDescription::ISite is2=
	StateDescription::ISite(is_between.j1,is_between.j2,is_orig.j1,is_orig.j2);
    
    sd.resize(2);
    sd[0] = is1;
    sd[1] = is2;
}

// ------------------------------------------------------------
// Move Iterator


HybEnsModel::MoveIterator::
MoveIterator(const StateDescription &origin,
	     const HybEnsModel &model,
	     bool with_double)
    :origin_(origin),
     model_(model),
     with_double_(with_double)
{
}


HybEnsModel::Move *
HybEnsModel::MoveIterator::firstMove() const {
    Move *m = new GrowShrinkMoveFL(*this);
    
    while( (m!=NULL) && (! m->first()) ) {
	Move *m2 = m->nextMoveType();
	delete m;
	m=m2;
    }
    return m;
}


HybEnsModel::Move *
HybEnsModel::MoveIterator::nextMove(Move *m) const {
    assert( m != NULL );

    if ( m->next() ) {
	return m;
    };

    do {
	Move *m2 = m->nextMoveType(); // get new move object of next move type
	delete m; // and delete old object
	if (m2==NULL) {return NULL;} // return NULL, if there is no next move type
	m=m2;
    } while( ! m->first() ); // there is no first move of the next move type 
    
    return m;
}
